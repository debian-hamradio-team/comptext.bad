// Process this file with AsciiDoc:
// Command: a2x.py --doctype manpage --format manpage --no-xmllint comptext.1.txt
:doctype: manpage
:man source: COMPTEXT
:man manual: COMPTEXT Man Page
:man date: {date}
:man version: Version 1.0.1

= comptext(1)

== NAME

comptext - GUI based tool to compare two text streams


== SYNOPSIS

*comptext* -  Comptext is used to compare two text streams. It's comparison
 function uses a linear-programming technique.  It computes total number of
 errors, the character error rate (CER) and the bit error rate (BER).


== BUGS

If you find a bug or suspect *'comptext'* is not acting as you think it should,
send an email with as much detail as possible to: <fldigi-devel@lists.sourceforge.net>


== AUTHORS
-----
Dave Freeze, W1HKJ, <w1hkj@w1hkj.com>
-----


== RESOURCES
-----
Project Site: ... <http://sourceforge.net/projects/fldigi/>
Main web site: .. <http://www.w1hkj.com>
-----


== DEBIAN MAINTAINERS
-----
Maintainer ..: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders ...: Greg Beam <ki7mt@yahoo.com>, Kamal Mostafa <kamal@whence.com>
IRC .........: OFTC #debian-hams, FREENODE: #ubuntu-hams
-----

